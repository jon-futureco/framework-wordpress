<?php
define( 'WP_DEBUG', true );
$shadow_var = 'false';

remove_filter('the_content', 'wpautop');

$template_dir = get_template_directory_uri();

add_action( 'after_setup_theme', 'future_and_co_framework_setup' );

function future_and_co_framework_setup() {
  load_theme_textdomain( 'future_and_co_framework', get_template_directory_uri() . '/languages' );
  add_theme_support( 'automatic-feed-links' );
  add_theme_support( 'post-thumbnails' );
  global $content_width;
  if ( ! isset( $content_width ) ) $content_width = 640;
  register_nav_menus( array(
    'main-menu' => 'Main Menu',
  ) );
}

add_action( 'wp_enqueue_scripts', 'future_and_co_framework_load_scripts' );

function future_and_co_framework_load_scripts() {
  wp_enqueue_style( 'main', get_template_directory_uri().'/stylesheets/screen.css');

  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'main', get_template_directory_uri().'/js/main.js' );
}

add_action( 'comment_form_before', 'future_and_co_framework_enqueue_comment_reply_script' );

function future_and_co_framework_enqueue_comment_reply_script() {
  if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}

add_filter( 'the_title', 'future_and_co_framework_title' );

function future_and_co_framework_title( $title ) {
  if ( $title == '' ) {
    return '&rarr;';
  } else {
    return $title;
  }
}

add_filter( 'wp_title', 'future_and_co_framework_filter_wp_title' );
function future_and_co_framework_filter_wp_title( $title ) {
  return $title . esc_attr( get_bloginfo( 'name' ) );
}

add_action( 'widgets_init', 'future_and_co_framework_widgets_init' );

function future_and_co_framework_widgets_init() {
  register_sidebar( array (
    'name' => __( 'Sidebar Widget Area', 'future_and_co_framework' ),
    'id' => 'primary-widget-area',
    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
    'after_widget' => "</li>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ) );
}

function future_and_co_framework_custom_pings( $comment ) {
  $GLOBALS['comment'] = $comment;
  ?>
  <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
  <?php
}

add_filter( 'get_comments_number', 'future_and_co_framework_comments_number' );

function future_and_co_framework_comments_number( $count ) {
  if ( !is_admin() ) {
    global $id;
    $comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
    return count( $comments_by_type['comment'] );
  } else {
    return $count;
  }
}

// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');

// Get shortcodes
require_once('shortcodes/lists.php');
require_once('shortcodes/icons.php');
require_once('shortcodes/buttons.php');
require_once('shortcodes/hr.php');
require_once('shortcodes/grid.php');
require_once('shortcodes/alert.php');

