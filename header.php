<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?php wp_title( ' | ', true, 'right' ); ?></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href="<?php echo get_stylesheet_uri(); ?>" media="screen, projection" rel="stylesheet" />

  <!--[if lt IE 9]><script src="js/html5shiv-printshiv.js" media="all"></script><![endif]-->

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <div id="wrapper">
    <header id="header" role="banner">
      <div class="central-content">

        <section id="branding">
          <div id="site-title"><?php if ( ! is_singular() ) { echo '<h1>'; } ?><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php esc_attr_e( get_bloginfo( 'name' ), 'future_and_co_framework' ); ?>" rel="home"><?php echo esc_html( get_bloginfo( 'name' ) ); ?></a><?php if ( ! is_singular() ) { echo '</h1>'; } ?></div>
          <div id="site-description"><?php bloginfo( 'description' ); ?></div>
        </section>

        <nav id="menu" role="navigation" class="navbar">
          <div class="navbar__inner">
            <?php
              wp_nav_menu( array(
                'theme_location'   => 'main-menu',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav',
                'items_wrap'        => '<ul id="%1$s" class="%2$s"><a class="navbar__brand" href="'.home_url().'">Home</a>%3$s</ul>',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker()
              ) );
            ?>
          </div>
        </nav>
      </div>
    </header>

    <div id="main-content" class="central-content">
      <div class="row">