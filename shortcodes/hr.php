<?php

function f_and_co_hr($atts) {

  extract(shortcode_atts(array(
      'type' => '',
  ), $atts));

  $hr = "<hr />";

  return $hr;
}

add_shortcode('hr', 'f_and_co_hr');

?>