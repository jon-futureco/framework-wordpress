<?php

function f_and_co_button($atts) {
  global $shadow_var;

  extract(shortcode_atts(array(
      'type' => 'button',
      'class' => '',
      'value' => 'Click',
      'shadow' => '',
      'link' => '#',
      'size' => '',
      'icon' => '',
      'icon_position' => ''
  ), $atts));

  $btn_class = "";

  if($class !== ""){
    $btn_class .= "btn-$class";
  }

  if($size !== ""){
    $btn_class = "btn-$size";
  }

  if($shadow === 'true'){
    $btn_class .= " btn-shadow";
    $btn_inner = "<div>$value</div>";
  } else if($shadow_var === 'true' & $shadow !== 'false') {
    $btn_class .= " btn-shadow";
    $btn_inner = "<div>$value</div>";
  } else {
    $btn_inner = "$value";
  }

  if ($type === "link") {
    $type = "a";
    $btn_link = " href='$link'";
  } else {
    $btn_link = "";
    if($type !== "submit"){
      $type = "button";
    }
  }

  if ($icon !== "") {
    // Generate icon code using icon shortcode function
    $btn_icon = f_and_co_icon( array("icon" => $icon, "icon_position" => $icon_position) );
    // Add icon to right or left of button text
    if ($icon_position === "right") {
      $btn_inner .= " ".$btn_icon;
    } else {
      $btn_inner = $btn_icon . " " . $btn_inner;
    }
  }

  if($type !== "submit"){
    $btn = "<$type class='btn $btn_class'$btn_link>$btn_inner</$type>";
  } else {
    $btn = "<input type='submit' class='btn $btn_class' value='$value' />";
  }

  return $btn;
}

add_shortcode('button', 'f_and_co_button');

?>