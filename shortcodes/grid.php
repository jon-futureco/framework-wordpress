<?php

function f_and_co_grid_row($atts, $content = null) {

  extract(shortcode_atts(array(
      'class' => '',
  ), $atts));

  if ($class !== "") {
    $row_class .= " $class";
  }

  $row = "<div class='row$row_class'>".do_shortcode($content)."</div>";

  return $row;
}

add_shortcode('grid_row', 'f_and_co_grid_row');

function f_and_co_grid_col($atts, $content = null) {

  extract(shortcode_atts(array(
      'class' => '',
      'size'  => 'full'
  ), $atts));

  $col_class = " ".$size;
  if ($class !== "") {
    $col_class .= " $class";
  }

  $row = "<div class='$col_class'>".do_shortcode($content)."</div>";

  return $row;
}

add_shortcode('grid_col', 'f_and_co_grid_col');

?>