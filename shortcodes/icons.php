<?php

function f_and_co_icon($atts) {

  extract(shortcode_atts(array(
      'icon' => '',
      'icon_class' => ''
  ), $atts));

  if ($icon !== "") {
    $fa_icon = "<i class='fa ";
    $fa_icon .= $icon . " " . $icon_class;
    $fa_icon .= "'></i>";
  } else {
    $fa_icon = "";
  }

  return $fa_icon;
}

add_shortcode('icon', 'f_and_co_icon');

function f_and_co_icon_list($atts, $content = null){
  extract(shortcode_atts(array(
      'icon' => '',
      'icon_class' => ''
  ), $atts));

  $icon_list = f_and_co_list(array("class" => "fa-ul"),$content);

  return $icon_list;
}

add_shortcode('icon_list', 'f_and_co_icon_list');

function f_and_co_icon_stack($atts, $content = null){

  extract(shortcode_atts(array(
      'class' => ''
  ), $atts));

  if($class !== ""){
    $class = " $class";
  }

  $icon_stack = "<span class='fa-stack$class'>".do_shortcode($content)."</span>";

  return $icon_stack;
}

add_shortcode('icon-stack','f_and_co_icon_stack');

?>