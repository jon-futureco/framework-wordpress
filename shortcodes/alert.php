<?php

function f_and_co_alert($atts, $content="null"){
  extract(shortcode_atts(array(
      'class' => '',
      'type' => '',
      'close' => 'false'
  ), $atts));

  if($class !== ""){
    $class = " $class";
  }

  if($type !== ""){
    $class .= " alert-$type";
  }

  if($close === "true"){
    $alert_close = "<button type='button' class='close' data-dismiss='alert'><i class='fa fa-times' aria-hidden='true'></i><span class='sr-only'>Close</span></button>";
    $class .= " alert-dismissible";
  } else {
    $alert_close = "";
  }

  $alert = "<div class='alert$class' role='alert'>$alert_close".do_shortcode($content)."</div>";

  return $alert;
}

add_shortcode('alert','f_and_co_alert');

?>