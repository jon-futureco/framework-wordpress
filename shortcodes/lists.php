<?php

function f_and_co_list($atts, $content = null){
  extract(shortcode_atts(array(
      'class' => ''
  ), $atts));

  if($class !== ""){
    $class = " class='$class'";
  }

  $list = "<ul$class>".do_shortcode($content)."</ul>";

  return $list;
}

add_shortcode('list', 'f_and_co_list');

function f_and_co_list_item($atts, $content = null){
  extract(shortcode_atts(array(
      'class' => ''
  ), $atts));

  if($class !== ""){
    $class = " class='$class'";
  }

  $list = "<li$class>".do_shortcode($content)."</li>";

  return $list;
}

add_shortcode('list_item', 'f_and_co_list_item');



?>